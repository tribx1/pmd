package com.jpg.pmd.webapp.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "pmd_file")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class File {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "name")
	private String name;

	@Column(name = "file_download_uri")
	private String fileDownloadUri;

	@Column(name = "file_type")
	private String fileType;

	@Column(name = "size")
	private long size;

	@ManyToOne(fetch = FetchType.LAZY)
	private User user;

	@OneToMany(mappedBy = "file")
	private Set<ErrorCheck> errorChecks;

	public File(String name, String fileDownloadUri, String fileType, long size, User user) {
		super();
		this.name = name;
		this.fileDownloadUri = fileDownloadUri;
		this.fileType = fileType;
		this.size = size;
		this.user = user;
	}

	
}
