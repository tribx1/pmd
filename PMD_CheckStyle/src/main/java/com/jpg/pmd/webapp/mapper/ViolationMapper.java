package com.jpg.pmd.webapp.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.jpg.pmd.webapp.dto.ViolationDto;
import com.jpg.pmd.webapp.entity.Violation;

@Mapper
public interface ViolationMapper {
	ViolationMapper INSTANCE = Mappers.getMapper(ViolationMapper.class);

	ViolationDto violationToViolationDto(Violation violation);
}
