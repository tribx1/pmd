/**
 * 
 */
package com.jpg.pmd.webapp.data;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author TriBX1
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DataReturnList<T> {

	private String message;

	private String success = "true";

	private List<T> data;
	
	private int totalPage;
	
	private long totalElement;
}
