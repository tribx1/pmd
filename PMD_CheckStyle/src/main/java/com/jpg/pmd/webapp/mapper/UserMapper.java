package com.jpg.pmd.webapp.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.jpg.pmd.webapp.dto.UserDto;
import com.jpg.pmd.webapp.entity.User;

@Mapper
public interface UserMapper {
	
	UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);
	@Mappings({
		@Mapping(target="userName", source="user.username"),
		@Mapping(target="passWord", source="user.password")
		})
	UserDto userToUserDto(User user);
}
