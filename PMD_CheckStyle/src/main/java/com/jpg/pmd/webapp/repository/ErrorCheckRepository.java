package com.jpg.pmd.webapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.jpg.pmd.webapp.entity.ErrorCheck;

@Repository
public interface ErrorCheckRepository extends JpaRepository<ErrorCheck, Long>{

}
