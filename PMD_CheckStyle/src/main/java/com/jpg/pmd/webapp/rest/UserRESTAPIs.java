package com.jpg.pmd.webapp.rest;

import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.jpg.pmd.webapp.data.DataReturnList;
import com.jpg.pmd.webapp.data.DataReturnOne;
import com.jpg.pmd.webapp.dto.FileUploadDto;
import com.jpg.pmd.webapp.dto.ViolationDto;
import com.jpg.pmd.webapp.entity.FileUpload;
import com.jpg.pmd.webapp.entity.User;
import com.jpg.pmd.webapp.entity.Violation;
import com.jpg.pmd.webapp.exception.NotFoundException;
import com.jpg.pmd.webapp.mapper.FileUploadMapper;
import com.jpg.pmd.webapp.mapper.UserMapper;
import com.jpg.pmd.webapp.mapper.ViolationMapper;
import com.jpg.pmd.webapp.service.FileUploadService;
import com.jpg.pmd.webapp.service.UserService;
import com.jpg.pmd.webapp.service.ViolationService;
import com.jpg.pmd.webapp.service.XmlService;
import com.jpg.pmd.webapp.service.impl.FileStorageService;

import net.sourceforge.pmd.PMD;

@RestController
@RequestMapping("api/v1/users")
@CrossOrigin(origins = "*", maxAge = 3600)
public class UserRESTAPIs {

//	private static final Logger logger = LoggerFactory.getLogger(UserRESTAPIs.class);

	@Autowired
	UserMapper userMapper;

	@Autowired
	UserService userService;

	@Autowired
	ViolationMapper violationMapper;

	@Autowired
	FileUploadMapper fileUploadMapper;

	@Autowired
	FileUploadService fileUploadService;

	@Autowired
	XmlService xmlService;

	@Autowired
	ViolationService violationService;

	@Autowired
	private FileStorageService fileStorageService;
	
	//@PreAuthorize("hasRole('USER')")
	@PostMapping("/{userName}/files")
	public ResponseEntity<DataReturnOne<FileUploadDto>> uploadFile(@RequestParam("file") MultipartFile file,
			@PathVariable("userName") String username) {
		String fileName = fileStorageService.storeFile(file);

		String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
				.path("/api/v1/user/file/downloadFile/1/").path(fileName).toUriString();
		User user = userService.findUserByUserName(username);
		FileUpload fileUpload = fileUploadService
				.addFile(new FileUpload(fileName, fileDownloadUri, file.getContentType(), file.getSize(), user));
		Resource resource = fileStorageService.loadFileAsResource(fileUpload.getName(), 1);
		String[] arguments = { "-d", "D:\\Users\\pmd\\uploads\\" + resource.getFilename(), "-f", "xml", "-R",
				"rulesets/java/quickstart.xml", "-r", "D:\\Users\\pmd\\reports\\report" + fileUpload.getId() + ".xml",
				"-no-cache" };
		PMD.run(arguments);
		xmlService.paresErrorCheck("D:\\Users\\pmd\\reports\\report" + fileUpload.getId() + ".xml", fileUpload);
		DataReturnOne<FileUploadDto> dataReturnOne = new DataReturnOne<FileUploadDto>();
		dataReturnOne.setMessage("Run pmd and upload file Success");
		dataReturnOne.setData(fileUploadMapper.fileUploadToFileUploadDto(fileUpload));
		return new ResponseEntity<>(dataReturnOne, HttpStatus.OK);
	}

	@GetMapping("/files/downloadFile/{type}/{fileName:.+}")
	public ResponseEntity<Resource> downloadFile(@PathVariable String fileName, @PathVariable int type) {
		Resource resource = null;
		if (type == 1) {
			resource = fileStorageService.loadFileAsResource(fileName, 1);
		} else {
			resource = fileStorageService.loadFileAsResource(fileName, 2);
		}

		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
				.body(resource);
	}

	@GetMapping("/{username}/files")
	public ResponseEntity<DataReturnList<FileUploadDto>> getAllFileByUserName(@PathVariable String username, @RequestParam int size, @RequestParam int page) {
		DataReturnList<FileUploadDto> dataReturnList = new DataReturnList<FileUploadDto>();
		Page<FileUpload> files ;
		Pageable pageable = PageRequest.of(page, size);
		try {
			files = fileUploadService.retrieveFileByUser(username, pageable);
		} catch (NotFoundException e) {
			dataReturnList.setMessage(e.getMessage());
			dataReturnList.setSuccess("false");
			dataReturnList.setData(null);
			return new ResponseEntity<>(dataReturnList, HttpStatus.NOT_FOUND);
		}
		dataReturnList.setMessage("Retrieve file success");
		dataReturnList
				.setData(files.getContent().stream().map(fileUploadMapper::fileUploadToFileUploadDto).collect(Collectors.toList()));
		dataReturnList.setTotalPage(files.getTotalPages());
		dataReturnList.setTotalElement(files.getTotalElements());
		return new ResponseEntity<>(dataReturnList, HttpStatus.OK);
	}
	
	@GetMapping("/files")
	public ResponseEntity<DataReturnList<FileUploadDto>> getAllFile(@RequestParam int size, @RequestParam int page) {
		DataReturnList<FileUploadDto> dataReturnList = new DataReturnList<FileUploadDto>();
		Page<FileUpload> files ;
		Pageable pageable = PageRequest.of(page, size);
		try {
			files = fileUploadService.retrieveAll(pageable);
		} catch (NotFoundException e) {
			dataReturnList.setMessage(e.getMessage());
			dataReturnList.setSuccess("false");
			dataReturnList.setData(null);
			return new ResponseEntity<>(dataReturnList, HttpStatus.NOT_FOUND);
		}
		dataReturnList.setMessage("Retrieve file success");
		dataReturnList
				.setData(files.getContent().stream().map(fileUploadMapper::fileUploadToFileUploadDto).collect(Collectors.toList()));
		dataReturnList.setTotalPage(files.getTotalPages());
		dataReturnList.setTotalElement(files.getTotalElements());
		return new ResponseEntity<>(dataReturnList, HttpStatus.OK);
	}	
	
	@GetMapping("/files/{fileId}/violations")
	public ResponseEntity<DataReturnList<ViolationDto>> getAllErrorCheckByFileId(@PathVariable int fileId, @RequestParam int size, @RequestParam int page) {
		DataReturnList<ViolationDto> dataReturnList = new DataReturnList<ViolationDto>();
		Page<Violation> violations ;
		Pageable pageable = PageRequest.of(page, size);
		try {
			violations = violationService.retrieveByFileID(fileId, pageable);
		} catch (NotFoundException e) {
			dataReturnList.setMessage(e.getMessage());
			dataReturnList.setSuccess("false");
			dataReturnList.setData(null);
			return new ResponseEntity<>(dataReturnList, HttpStatus.NOT_FOUND);
		}
		dataReturnList.setMessage("Retrieve violation success");
		dataReturnList.setData(
				violations.getContent().stream().map(violationMapper::violationToViolationDto).collect(Collectors.toList()));
		dataReturnList.setTotalPage(violations.getTotalPages());
		dataReturnList.setTotalElement(violations.getTotalElements());
		return new ResponseEntity<>(dataReturnList, HttpStatus.OK);
	}

	@GetMapping("/violations")
	public ResponseEntity<DataReturnList<ViolationDto>> getAllViolation(@RequestParam int size, @RequestParam int page) {
		DataReturnList<ViolationDto> dataReturnList = new DataReturnList<ViolationDto>();
		Page<Violation> violations;
		Pageable pageable = PageRequest.of(page, size);
		try {
			violations = violationService.retrieveAll(pageable);
		} catch (NotFoundException e) {
			dataReturnList.setMessage(e.getMessage());
			dataReturnList.setSuccess("false");
			dataReturnList.setData(null);
			return new ResponseEntity<>(dataReturnList, HttpStatus.NOT_FOUND);
		}		
		dataReturnList.setMessage("Retrieve violation success");
		dataReturnList.setData(
				violations.getContent().stream().map(violationMapper::violationToViolationDto).collect(Collectors.toList()));
		dataReturnList.setTotalPage(violations.getTotalPages());
		return new ResponseEntity<>(dataReturnList, HttpStatus.OK);
	}

	
}
