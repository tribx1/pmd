package com.jpg.pmd.webapp.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.jpg.pmd.webapp.entity.FileUpload;

public interface FileUploadService {
	FileUpload addFile(FileUpload fileUpload);
	Page<FileUpload> retrieveAll(Pageable pageable);
	Page<FileUpload> retrieveFileByUser(String username, Pageable pageable);
}
