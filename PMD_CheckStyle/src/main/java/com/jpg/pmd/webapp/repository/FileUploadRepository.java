package com.jpg.pmd.webapp.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.jpg.pmd.webapp.entity.FileUpload;
import com.jpg.pmd.webapp.entity.User;

@Repository
public interface FileUploadRepository extends PagingAndSortingRepository<FileUpload, Integer> {
	Page<FileUpload> findByUser(User user,  Pageable pageable);
	Page<FileUpload> findAll(Pageable pageable);
}
