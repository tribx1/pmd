package com.jpg.pmd.webapp.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.jpg.pmd.webapp.entity.FileUpload;
import com.jpg.pmd.webapp.entity.Violation;
import com.jpg.pmd.webapp.exception.NotFoundException;
import com.jpg.pmd.webapp.repository.FileUploadRepository;
import com.jpg.pmd.webapp.repository.ViolationRepository;
import com.jpg.pmd.webapp.service.ViolationService;

@Service
public class ViolationServiceImpl implements ViolationService {

	@Autowired
	ViolationRepository violationRepository;
	
	@Autowired
	FileUploadRepository fileUploadRepository;
	
	@Override
	public Page<Violation> retrieveAll(Pageable pageable) {
		Page<Violation> violationList;
		violationList =  violationRepository.findAll(pageable);
		if(violationList.isEmpty()) {
			throw new NotFoundException("Violation List not found");
		}
		return violationList;
	}

	@Override
	public Page<Violation> retrieveByFileID(int id, Pageable pageable) {
		Page<Violation> violationList ;	
		Optional<FileUpload> fileOptional = fileUploadRepository.findById(id);
		if(!fileOptional.isPresent()) {
			throw new NotFoundException("File not found");
		}
		violationList = violationRepository.findByFileUpload(fileOptional.get(), pageable);
		if(violationList.isEmpty()) {
			throw new NotFoundException("Violation List not found");
		}
		return violationList;
	}

	@Override
	public Violation addViolation(Violation violation) {		
		return violationRepository.save(violation);
	}

}
