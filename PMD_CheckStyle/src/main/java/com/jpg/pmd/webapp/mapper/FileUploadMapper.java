package com.jpg.pmd.webapp.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.jpg.pmd.webapp.dto.FileUploadDto;
import com.jpg.pmd.webapp.entity.FileUpload;

@Mapper
public interface FileUploadMapper {
	FileUploadMapper INSTANCE = Mappers.getMapper(FileUploadMapper.class);

	@Mappings({ @Mapping(target = "username", source = "fileUpload.user.username") })
	FileUploadDto fileUploadToFileUploadDto(FileUpload fileUpload);
}
