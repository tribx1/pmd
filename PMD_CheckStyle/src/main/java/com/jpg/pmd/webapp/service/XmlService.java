package com.jpg.pmd.webapp.service;


import java.util.List;

import com.jpg.pmd.webapp.entity.FileUpload;
import com.jpg.pmd.webapp.entity.Violation;

public interface XmlService {
	List<Violation> paresErrorCheck(String path,  FileUpload fileUpload);
}
