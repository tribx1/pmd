package com.jpg.pmd.webapp.service.impl;

import java.io.File;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.jpg.pmd.webapp.entity.FileUpload;
import com.jpg.pmd.webapp.entity.Violation;
import com.jpg.pmd.webapp.repository.ViolationRepository;
import com.jpg.pmd.webapp.service.XmlService;



@Service
public class XmlServiceImpl implements XmlService {

	@Autowired
	ViolationRepository violationRepository;

	@Override
	public List<Violation> paresErrorCheck(String path, FileUpload fileUpload) {
		try {

			File fXmlFile = new File(path);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);

			doc.getDocumentElement().normalize();
			NodeList nList = doc.getElementsByTagName("file");
			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eFile = (Element) nNode;
					NodeList nViolation = eFile.getElementsByTagName("violation");
					for (int j = 0; j < nViolation.getLength(); j++) {
						Node nNodeViolation = nViolation.item(j);
						if (nNodeViolation.getNodeType() == Node.ELEMENT_NODE) {
							Element eViolation = (Element) nNodeViolation;
							Violation violation = new Violation();
							violation.setFileName(eFile.getAttribute("name"));
							violation.setClassName(eViolation.getAttribute("class"));
							violation.setPriority(Integer.parseInt(eViolation.getAttribute("priority")));
							violation.setExternalInfoUrl(eViolation.getAttribute("externalInfoUrl"));
							violation.setPackageName(eViolation.getAttribute("package"));
							violation.setRuleSet(eViolation.getAttribute("ruleset"));
							violation.setRule(eViolation.getAttribute("rule"));
							violation.setBeginLine(Integer.parseInt(eViolation.getAttribute("beginline")));
							violation.setEndLine(Integer.parseInt(eViolation.getAttribute("endline")));
							violation.setBeginColumn(Integer.parseInt(eViolation.getAttribute("begincolumn")));
							violation.setEndColumn(Integer.parseInt(eViolation.getAttribute("endcolumn")));
							violation.setMessage(eViolation.getTextContent());
							violation.setFileUpload(fileUpload);
							violationRepository.save(violation);
						}
					}

				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
