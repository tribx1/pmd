package com.jpg.pmd.webapp.repository;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.jpg.pmd.webapp.entity.FileUpload;
import com.jpg.pmd.webapp.entity.Violation;

@Repository
public interface ViolationRepository extends PagingAndSortingRepository<Violation, Integer>{
	Page<Violation> findByFileUpload(FileUpload fileUpload, Pageable pageable);
	Page<Violation> findAll(Pageable pageable);
}
