package com.jpg.pmd.webapp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "pmd_error")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ErrorCheck {
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "rule")
	private String rule;

	@Column(name = "beginLine")
	private int beginLine;
	
	@Column(name = "endLine")
	private int endLine;
	
	@Column(name = "beginColumn")
	private int beginColumn;
	
	@Column(name = "endColumn")
	private int endcolumn;

	@Column(name = "priority")
	private int priority;

	@ManyToOne(fetch = FetchType.LAZY)
	private File file;
}
