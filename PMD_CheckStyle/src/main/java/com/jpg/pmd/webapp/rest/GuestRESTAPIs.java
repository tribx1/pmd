package com.jpg.pmd.webapp.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jpg.pmd.webapp.dto.JwtResponseDto;
import com.jpg.pmd.webapp.entity.User;
import com.jpg.pmd.webapp.mapper.UserMapper;
import com.jpg.pmd.webapp.security.jwt.JwtProvider;
import com.jpg.pmd.webapp.service.UserService;

@RestController
@RequestMapping("api/v1/guest")
@CrossOrigin(origins = "*", maxAge = 3600)
public class GuestRESTAPIs {

	@Autowired
	UserMapper userMapper;

	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	JwtProvider jwtProvider;

	@Autowired
	UserService userService;

	@PostMapping("/signin")
	public JwtResponseDto authenticateUser(@RequestBody User user) {
		Authentication authentication = authenticationManager
				.authenticate(new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);

		String jwt = jwtProvider.generateJwtToken(authentication);
		UserDetails userDetails = (UserDetails) authentication.getPrincipal();
		JwtResponseDto jwtRes = new JwtResponseDto(jwt,"Bearer" ,userDetails.getUsername(), userDetails.getAuthorities());

		return jwtRes;
	}

	@PostMapping("/users")
	public User createUser(@RequestBody User newuser) {
		bCryptPasswordEncoder = new BCryptPasswordEncoder();
		newuser.setPassword(bCryptPasswordEncoder.encode(newuser.getPassword()));
		User createUser = userService.registerUser(newuser);
		if (createUser != null) {
			return createUser;
		} 
		return null;
	}
}
