package com.jpg.pmd.webapp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "pmd_violation")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Violation {
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "file_name")
	private String fileName;
	
	@Column(name = "class_name")
	private String className;
	
	@Column(name = "priority")
	private int priority;
	
	@Column(name = "external_info_url")
	private String externalInfoUrl;
	
	@Column(name = "package_name")
	private String packageName;
	
	@Column(name = "rule_set")
	private String ruleSet;
	
	@Column(name = "rule")
	private String rule;
	
	@Column(name = "begin_line")
	private int beginLine;
	
	@Column(name = "end_line")
	private int endLine;
	
	@Column(name = "begin_column")
	private int beginColumn;

	@Column(name = "end_column")
	private int endColumn;
	
	@Column(name = "message")
	private String message;

	@ManyToOne(fetch = FetchType.LAZY)
	private FileUpload fileUpload;

}
