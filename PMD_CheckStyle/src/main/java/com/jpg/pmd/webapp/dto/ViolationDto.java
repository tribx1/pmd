package com.jpg.pmd.webapp.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ViolationDto {
	private String fileName;
	private String className;
	private int priority;
	private String externalInfoUrl;
	private String packageName;
	private String ruleSet;
	private String rule;
	private int beginLine;
	private int endLine;
	private int beginColumn;
	private int endColumn;
	private String message;

}
