package com.jpg.pmd.webapp.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jpg.pmd.webapp.entity.ErrorCheck;
import com.jpg.pmd.webapp.repository.ErrorCheckRepository;
import com.jpg.pmd.webapp.service.ErrorCheckService;

@Service
public class ErrorCheckServiceImpl implements ErrorCheckService{

	@Autowired
	ErrorCheckRepository errorCheckRepository;
	
	@Override
	public ErrorCheck addErrorCheck(ErrorCheck errorCheck) {		
		return errorCheckRepository.save(errorCheck);
	}

}
