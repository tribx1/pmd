package com.jpg.pmd.webapp.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FileUploadDto {
	private int id;
	private String name;
	private String fileDownloadUri;
	private String fileType;
	private long size;
	private String username;
}
