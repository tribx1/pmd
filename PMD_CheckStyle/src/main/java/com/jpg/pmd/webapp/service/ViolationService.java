package com.jpg.pmd.webapp.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.jpg.pmd.webapp.entity.Violation;

public interface ViolationService {
	Violation addViolation(Violation violation);

	Page<Violation> retrieveAll(Pageable pageable);

	Page<Violation> retrieveByFileID(int id, Pageable pageable);
}
