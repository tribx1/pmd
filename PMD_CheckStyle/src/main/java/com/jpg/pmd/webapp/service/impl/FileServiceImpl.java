package com.jpg.pmd.webapp.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jpg.pmd.webapp.entity.File;
import com.jpg.pmd.webapp.repository.FileRepository;
import com.jpg.pmd.webapp.service.FileService;

@Service
public class FileServiceImpl implements FileService{

	@Autowired
	FileRepository fileRepository;
	
	@Override
	public File addFile(File file) {		
		return fileRepository.save(file);
	}

}
