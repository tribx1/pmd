package com.jpg.pmd.webapp.service;

import com.jpg.pmd.webapp.entity.User;

public interface UserService {
	User findUserByUserNameAndPassword(String username, String password);
	User findUserByUserName(String username);
	User registerUser(User user);
}
