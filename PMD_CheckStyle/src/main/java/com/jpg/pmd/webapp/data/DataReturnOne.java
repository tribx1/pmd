/**
 * 
 */
package com.jpg.pmd.webapp.data;

/**
 * @author TriBX1
 *
 */
public class DataReturnOne<T> {
	
	private String message;
	
	private String success = "true";
	
	private T data;

	public DataReturnOne() {
		super();
	}

	public DataReturnOne(String message, String success, T data) {
		super();
		this.message = message;
		this.success = success;
		this.data = data;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getSuccess() {
		return success;
	}

	public void setSuccess(String success) {
		this.success = success;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}
	
	

}
