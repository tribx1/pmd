package com.jpg.pmd.webapp.service.impl;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jpg.pmd.webapp.entity.Role;
import com.jpg.pmd.webapp.entity.User;
import com.jpg.pmd.webapp.exception.NotFoundException;
import com.jpg.pmd.webapp.repository.RoleRepository;
import com.jpg.pmd.webapp.repository.UserRepository;
import com.jpg.pmd.webapp.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	@Override
	public User findUserByUserNameAndPassword(String username, String password) {
		Optional<User> user = userRepository.findByUsernameAndPassword(username, password);
		if (!user.isPresent())
			throw new NotFoundException("User Not Found!!!");
		return user.get();
	}

	@Override
	public User findUserByUserName(String username) {
		Optional<User> user = userRepository.findByUsername(username);
		if (!user.isPresent())
			throw new NotFoundException("User Not Found!!!");
		return user.get();
	}

	@Override
	public User registerUser(User user) {
		User userCreate = userRepository.save(user);
		Role role = roleRepository.findByName("ROLE_USER").get();
		Set<Role> roles = new HashSet<>();
		roles.add(role);
		userCreate.setRoles(roles);
		return userRepository.save(userCreate);
	}

}
