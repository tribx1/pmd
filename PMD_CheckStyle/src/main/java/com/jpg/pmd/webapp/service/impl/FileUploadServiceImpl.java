package com.jpg.pmd.webapp.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.jpg.pmd.webapp.entity.FileUpload;
import com.jpg.pmd.webapp.entity.User;
import com.jpg.pmd.webapp.exception.NotFoundException;
import com.jpg.pmd.webapp.repository.FileUploadRepository;
import com.jpg.pmd.webapp.repository.UserRepository;
import com.jpg.pmd.webapp.service.FileUploadService;

@Service
public class FileUploadServiceImpl implements FileUploadService{

	@Autowired
	FileUploadRepository fileUploadRepository;
	
	@Autowired
	UserRepository userRepository;
	
	@Override
	public FileUpload addFile(FileUpload fileUpload) {		
		return fileUploadRepository.save(fileUpload);
	}

	@Override
	public Page<FileUpload> retrieveAll(Pageable pageable) {
		Page<FileUpload> files;
		files = fileUploadRepository.findAll(pageable);
		if (files.isEmpty()) {
			throw new NotFoundException("List File not found");
		}
		return files;
	}

	@Override
	public Page<FileUpload> retrieveFileByUser(String username, Pageable pageable) {
		Optional<User> userOptional = userRepository.findByUsername(username);
		if(!userOptional.isPresent()) {
			throw new NotFoundException("User not found");
		}
		Page<FileUpload> files;
		files = fileUploadRepository.findByUser(userOptional.get(), pageable);
		if (files.isEmpty()) {
			throw new NotFoundException("List File not found");
		}
		return files;
	}

}
