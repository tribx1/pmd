package com.jpg.pmd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import com.jpg.pmd.webapp.config.FileStorageProperties;

@SpringBootApplication
@EnableConfigurationProperties({ FileStorageProperties.class })
public class PmdCheckStyleApplication {

	public static void main(String[] args) {
		SpringApplication.run(PmdCheckStyleApplication.class, args);

	}

}
